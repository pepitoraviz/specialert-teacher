import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { SMS } from '@ionic-native/sms';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { QRScanner } from '@ionic-native/qr-scanner';
import { ContactProvider } from '../providers/contact/contact';
import { ContactsPage } from '../pages/contacts/contact';
import { ProfileProvider } from '../providers/profile/profile';
import { MessagePage } from '../pages/message/message';
import { Flashlight } from '@ionic-native/flashlight';

const socketConfig: SocketIoConfig = { url: 'https://specialert.herokuapp.com' };
@NgModule({
  declarations: [MyApp, AboutPage, ContactPage, HomePage, ContactsPage, MessagePage, TabsPage],
  imports: [BrowserModule, SocketIoModule.forRoot(socketConfig), IonicModule.forRoot(MyApp)],
  bootstrap: [IonicApp],
  entryComponents: [MyApp, AboutPage, ContactPage, HomePage, ContactsPage, MessagePage, TabsPage],
  providers: [
    StatusBar,
    SplashScreen,
    QRScanner,
    SMS,
    ProfileProvider,
    Flashlight,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ContactProvider
  ]
})
export class AppModule { }
