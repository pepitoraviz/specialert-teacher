import { Component } from "@angular/core";

import { ContactPage } from "../contact/contact";
import { HomePage } from "../home/home";
import { ContactsPage } from '../contacts/contact';
import { MessagePage } from '../message/message';
@Component({
  templateUrl: "tabs.html"
})
export class TabsPage {
  tabMessageRoot = MessagePage;
  tab1Root = HomePage;
  tab3Root = ContactPage;
  tab2 = ContactsPage;
  constructor() { }
}
