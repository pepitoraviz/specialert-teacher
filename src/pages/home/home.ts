import { Component, ViewChild, ElementRef } from "@angular/core";
import { NavController } from "ionic-angular";
import { Socket } from "ng-socket-io";
declare var google;
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  @ViewChild("map") mapElement: ElementRef;
  map: any;
  bounds: any;
  studentOnAlert: any[] = [];
  markers: any[] = [];
  geoCoder: any;
  infoWindow: any;
  constructor(public navCtrl: NavController, private socket: Socket) {
    this.bounds = new google.maps.LatLngBounds();
    this.geoCoder = new google.maps.Geocoder();
    this.infoWindow = new google.maps.InfoWindow();
  }
  ionViewDidEnter() {
    this.subscribeOnAlert();
  }
  ionViewDidLoad() {
    this.loadMap();
  }

  loadMap() {
    let latLng = new google.maps.LatLng(14.560008, 121.077165);
    let mapOptions = {
      center: latLng,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
  }

  renderMarkers() {
    if (Array.isArray(this.studentOnAlert) && Object.keys(this.studentOnAlert).length > 0) {
      Object.keys(this.studentOnAlert).map((pos: any) => {
        if ("undefined" === typeof this.markers[pos]) {
          const { lat, lng } = this.studentOnAlert[pos]["pos"];
          const marker = new google.maps.Marker({
            map: this.map,
            position: { lat, lng }
          });
          this.markers[pos] = marker;
          this.bounds.extend(marker.getPosition());
          this.addInfoWindow(this.markers[pos], this.studentOnAlert[pos]);
        } else {
          const { lat, lng } = this.studentOnAlert[pos]["pos"];
          const latLng = new google.maps.LatLng({ lat, lng });
          this.markers[pos].setPosition(latLng);
          this.bounds.extend(this.markers[pos].getPosition());
          this.addInfoWindow(this.markers[pos], this.studentOnAlert[pos]);
        }
      });
    }
  }

  async addInfoWindow(marker, info) {
    const fullName = `${info.profile.firstName} ${info.profile.lastName}`;
    const { lat, lng } = info.pos;
    google.maps.event.addListener(marker, "click", async () => {
      this.infoWindow.close();
      const address = await this.getGeoCode({ lat, lng });
      this.infoWindow.setContent(`<span>${fullName}</span><br/><span>${address}</span>`);
      this.infoWindow.open(this.map, marker);
    });
  }
  subscribeOnAlert() {
    this.socket.on("student:alert", (info: any) => {
      console.log("[ALERT RECEIVED]: ", info);
      this.studentOnAlert[info.profile.udid] = info;
      this.renderMarkers();
      this.map.fitBounds(this.bounds);
    });
  }

  getGeoCode(latlng) {
    return new Promise((resolve, reject) => {
      try {
        this.geoCoder.geocode({ location: latlng }, (result, status) => {
          const formatedAddress = result[0].formatted_address;
          resolve(formatedAddress);
        });
      } catch (error) {
        resolve("qouta exceeded");
      }
    });
  }
}
