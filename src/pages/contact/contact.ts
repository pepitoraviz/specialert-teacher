import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { ContactProvider } from '../../providers/contact/contact';
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  isQRAuthorized: boolean = false;
  startScanning: boolean = false;
  contacts: any[] = [];
  constructor(
    public platform: Platform,
    private qr: QRScanner,
    private contactServ: ContactProvider
  ) {
    this.prepareQR();
  }
  async ngOnInit() {
    const contacts = await (<Promise<any[]>>this.contactServ.getContact());
    this.contacts = contacts;
  }
  prepareQR() {
    console.log('[PREPARE QR]');
    this.platform.ready().then(() => {
      this.qr.prepare().then((status: QRScannerStatus) => {
        if (status.authorized) {
          this.isQRAuthorized = true;
          this.qr.show();
          this.qr.pausePreview();
          console.log('[camera authorized]');
        } else if (status.denied) {
          this.qr.openSettings();
        }
      });
    });
  }
  addStudent() {
    this.qr
      .resumePreview()
      .then(() => this.showCamera())
      .then(() => this.handleOnScan())
      .catch(err => alert(err));
  }

  handleOnScan() {
    const scanObservable = this.qr.scan().subscribe(async (text: string) => {
      const contact = JSON.parse(text);
      const contactAlreadyExist = this.checkIfContactAlreadyExist(contact.udid);
      if (false === contactAlreadyExist) {
        this.contacts.push(contact);
      }
      await this.contactServ.setContact(this.contacts);
      this.hideCamera().then(() => {
        console.log('[CAMERA HIDE]');
        console.log('[IS SCANNING]', this.startScanning);
        this.qr.pausePreview();

        scanObservable.unsubscribe();
      });
    });
  }
  checkIfContactAlreadyExist(udid: string) {
    if (Array.isArray(this.contacts)) {
      const udids = this.contacts.map(contact => contact.udid);
      return udids.indexOf(udid) === -1 ? false : true;
    }
    return false;
  }
  showCamera() {
    return new Promise(resolve => {
      window.document.querySelector('ion-app').classList.add('transparentBody');
      this.qr.resumePreview();
      console.log('[camera showed]');
      this.toggleScanning();
      resolve();
    });
  }
  hideCamera() {
    return new Promise(resolve => {
      this.toggleScanning();
      window.document.querySelector('.scanner-container').classList.add('hidden');
      window.document.querySelector('ion-app').classList.remove('transparentBody');
      this.qr.hide();
      resolve();
    });
  }
  toggleScanning() {
    this.startScanning = !this.startScanning;
  }
}
