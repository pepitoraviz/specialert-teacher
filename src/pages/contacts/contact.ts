import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { ProfileProvider } from '../../providers/profile/profile';
export interface Contacts {
  contactName: string;
  contactNumber: string;
  default: boolean;
}
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html'
})
export class ContactsPage {
  contacts: Array<Contacts> = [];
  constructor(
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    private profileServ: ProfileProvider
  ) { }

  async ionViewWillEnter() {
    this.contacts = await this.profileServ.getContacts();
  }
  addContact() {
    const _self = this;
    console.log(_self.contacts);
    const prompt = this.alertCtrl.create({
      title: 'Input Contact',
      inputs: [
        { name: 'contactName', placeholder: 'Enter contact name' },
        { name: 'contactNumber', placeholder: 'Enter contact number', type: 'number' }
      ],
      buttons: [
        { text: 'Cancel' },
        {
          text: 'Save',
          handler: (data: Contacts) => {
            data.default = false;
            if (data.contactName && data.contactNumber) {
              _self.contacts.push(data);
              _self.profileServ.addContact(this.contacts);
            }
          }
        }
      ]
    });
    prompt.present();
  }
  editContact(ind) {
    let contact: Contacts = this.contacts[ind];
    const prompt = this.alertCtrl.create({
      title: 'Input Contact',
      inputs: [
        { name: 'contactName', placeholder: 'Enter contact name', value: contact.contactName },
        { name: 'contactNumber', placeholder: 'Enter contact name', value: contact.contactNumber }
      ],
      buttons: [
        { text: 'Cancel' },
        {
          text: 'Save',
          handler: (data: Contacts) => {
            if (data.contactName && data.contactNumber) {
              this.contacts[ind].contactName = data.contactName;
              this.contacts[ind].contactNumber = data.contactNumber;
              this.profileServ.addContact(this.contacts);
            }
          }
        }
      ]
    });
    prompt.present();
  }
}
