import { Injectable } from '@angular/core';
interface IContacts {
  firstName: String;
  lastName: String;
  udid: String;
}
@Injectable()
export class ContactProvider {
  constructor() {}

  setContact(contacts: IContacts[]) {
    return new Promise(resolve => {
      localStorage.setItem('tcontacts', JSON.stringify(contacts));
      resolve();
    });
  }

  getContact() {
    return new Promise((resolve, reject) => {
      const stringConts = localStorage.getItem('tcontacts');
      if (stringConts && stringConts.length > 0) {
        const contacts: IContacts[] = JSON.parse(stringConts);
        resolve(contacts);
      } else {
        resolve([]);
      }
    });
  }
}
